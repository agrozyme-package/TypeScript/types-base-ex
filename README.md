# types-base-ex

TypeScript definitions for BaseEx

- Use this package, to create folder `@types/base-ex` under the source folder and put the code in `index.d.ts`

```ts
declare module 'base-ex' {
  export * from '@agrozyme/types-base-ex';
}
```

- Can use `new` classes by `import {} from 'base-ex'`

  - Base1
  - Base16
  - Base32
  - Base58
  - Base64
  - UUencode
  - Base85
  - Base91
  - LEB128
  - Ecoji
  - Base2048
  - SimpleBase
  - BasePhi
  - ByteConverter
  - BaseEx

- Other declare class always use for type hint
