import { BaseTemplate } from './core';
import { SmartInput, SmartOutput, SmartOutputType } from './io-handlers';

export declare const DEFAULT_INPUT_HANDLER: typeof SmartInput;
export declare const DEFAULT_OUTPUT_HANDLER: typeof SmartOutput;

export interface BaseInputHandler {
  toBytes(input: any): [Uint8Array, boolean, string];
}

export interface BaseOutputHandler {
  typeList: string[];
  getType(type: string): string;
  compile(Uint8ArrayOut: Uint8Array, type: string): any;
}

export interface BaseParameters extends Record<string, any> {
  decimalMode: boolean;
  integrity: boolean;
  littleEndian: boolean;
  numberMode: boolean;
  options: Record<string, any>;
  outputType: SmartOutputType;
  padding: boolean;
  signed: boolean;
  upper: boolean;
  version: string;
}

export declare class SignError extends TypeError {
  name: 'SignError';
  constructor();
}

export declare class DecodingError extends TypeError {
  name: 'DecodingError';
  constructor(char: string, msg?: string);
}

export declare class Utils<T extends BaseTemplate> {
  root: T;
  converterArgs: Partial<BaseParameters>;
  inputHandler?: BaseInputHandler;
  outputHandler?: BaseOutputHandler;

  constructor(main: T);

  setIOHandlers(inputHandler?: BaseInputHandler, outputHandler?: BaseOutputHandler);

  toSignedStr(output: string, negative: boolean): string;

  extractSign(input: string): [string, boolean];

  validateArgs<U extends BaseTemplate>(args: string[], initial?: boolean): U;

  signError(): void;

  wrapOutput(output: string, cols?: number): string;

  normalizeInput(input: any, keepWS?: boolean): string;
}
