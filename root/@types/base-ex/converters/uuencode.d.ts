import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class UUencode<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {
  version: 'default' | 'original' | 'xx';
}
export default UUencode;
