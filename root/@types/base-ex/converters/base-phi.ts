import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class BasePhi<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {}
export default BasePhi;
