import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class Base85<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {
  version: 'adobe' | 'ascii85' | 'rfc1924' | 'z85';
}

export default Base85;
