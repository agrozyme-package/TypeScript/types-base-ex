import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class Base16<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {}
export default Base16;
