import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class Base64<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {
  version: 'default' | 'urlsafe';
}

export default Base64;
