import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class Ecoji<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {
  version: 'emojis_v1' | 'emojis_v2';
}

export default Ecoji;
