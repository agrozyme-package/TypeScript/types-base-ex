import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class SimpleBase<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {}
export default SimpleBase;
