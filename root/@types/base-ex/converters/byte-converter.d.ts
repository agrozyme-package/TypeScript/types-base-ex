import { BaseAbstract, BaseDecodeType } from '../core';
import { SmartInputType, SmartOutputCompileKey } from '../io-handlers';

declare class ByteConverter<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {
  constructor(...args: string[]);
  encode(input: SmartInputType | SmartInputType[], ...args: string[]): Uint8Array;
  decode(input: Int8Array, ...args: string[]): BaseDecodeType;
}

export default ByteConverter;
