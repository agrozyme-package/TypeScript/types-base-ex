import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class LEB128<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {
  version: 'default' | 'hex';
}
export default LEB128;
