import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class Base32<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {
  version: 'crockford' | 'rfc3548' | 'rfc4648' | 'zbase32';
}

export default Base32;
