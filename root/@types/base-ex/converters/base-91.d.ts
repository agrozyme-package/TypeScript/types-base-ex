import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class Base91<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {}
export default Base91;
