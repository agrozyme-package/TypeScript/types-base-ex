import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class Base1<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {
  version: 'all' | 'sequence' | 'default' | 'tmark';
}

export default Base1;
