import { BaseAbstract } from '../core';
import { SmartOutputCompileKey } from '../io-handlers';

declare class Base58<T extends SmartOutputCompileKey = 'buffer'> extends BaseAbstract<T> {
  version: 'default' | 'bitcoin' | 'flickr';
}

export default Base58;
