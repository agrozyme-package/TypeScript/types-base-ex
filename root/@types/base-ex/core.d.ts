import { SmartInputType, SmartOutputCompileKey, SmartOutputCompileReturn } from './io-handlers';
import { BaseParameters, Utils } from './utils';

export type BigintValue = bigint | boolean | number | string;
//export type BigintValue = Parameters<BigIntConstructor>[0];

export type ReplacerCallback = (frame: string, zeroPadding: number) => string;
export type ReplacerMakerCallback = <T extends BaseParameters = BaseParameters>(setting: T) => ReplacerCallback;

export interface PostEncodeParameter<T extends BaseParameters = BaseParameters> {
  inputBytes: Uint8Array;
  output: string;
  settings: T;
  zeroPadding: number;
  type: 'int' | 'float' | 'bytes';
}
export type PostEncodeCallback = <T extends BaseParameters = BaseParameters>(
  parameter: PostEncodeParameter<T>
) => string;

export interface PreDecodeParameter<T extends BaseParameters = BaseParameters> {
  input: string;
  settings: T;
}
export type PreDecodeCallback = <T extends BaseParameters = BaseParameters>(parameter: PreDecodeParameter<T>) => string;

export interface PostDecodeParameter<T extends BaseParameters = BaseParameters> extends PreDecodeParameter<T> {
  output: any;
}
export type PostDecodeCallback = <T extends BaseParameters = BaseParameters>(
  parameter: PostDecodeParameter<T>
) => Uint8Array;

export type BaseDecodeReturn = SmartOutputCompileReturn[SmartOutputCompileKey];

export declare class BaseConverter {
  radix: number;
  bsEnc: number;
  bsDec: number;
  decPadVal: number;
  powers: Record<number, bigint>;

  constructor(radix: number, bsEnc?: number, bsDec?: number, decPadVal?: number);

  static guessBS(radix: number): [number, number];

  encode(
    inputBytes: Uint8Array,
    charset: string,
    littleEndian?: boolean,
    replacer?: ReplacerCallback
  ): [string, number];

  decode(
    inputBaseStr: string,
    charset: string,
    padSet?: string[],
    integrity?: boolean,
    littleEndian?: boolean
  ): Uint8Array;

  padBytes(charCount: number): number;
  padChars(byteCount: number): number;
  pow(n: BigintValue): bigint;
  divmod(x: BigintValue, y: BigintValue): [bigint, bigint];
}

export declare class BaseTemplate extends BaseParameters {
  charsets: Record<string, string[]>;
  frozenCharsets: boolean;
  hasDecimalMode: boolean;
  hasSignedMode: boolean;
  padCharAmount: number;
  padChars: Record<string, string[]>;
  nonASCII: boolean;
  utils?: Utils<this>;

  isMutable: {
    integrity: boolean;
    littleEndian: boolean;
    padding: boolean;
    signed: boolean;
    upper: boolean;
  };

  constructor(appendUtils?: boolean);

  encode<T extends BaseParameters = BaseParameters>(
    input: SmartInputType | SmartInputType[],
    replacerFN: ReplacerMakerCallback<T> | null,
    postEncodeFN: PostEncodeCallback<T> | null,
    ...args: string[]
  ): string;

  decode<T extends BaseParameters = BaseParameters>(
    input: any,
    preDecodeFN: PreDecodeCallback<T> | null,
    postDecodeFN: PostDecodeCallback<T> | null,
    keepNL: boolean,
    ...args: string[]
  ): BaseDecodeReturn;
}

export declare abstract class BaseAbstract<T extends SmartOutputCompileKey = 'buffer'> extends BaseTemplate {
  version: 'default';
  constructor(...args: string[]);
  encode(input: SmartInputType | SmartInputType[], ...args: string[]): string;
  decode(input: any, ...args: string[]): SmartOutputCompileReturn[T];
}
