import Base1 from './converters/base-1';
import Base16 from './converters/base-16';
import Base2048 from './converters/base-2048';
import Base32 from './converters/base-32';
import Base58 from './converters/base-58';
import Base64 from './converters/base-64';
import Base85 from './converters/base-85';
import Base91 from './converters/base-91';
import BasePhi from './converters/base-phi';
import ByteConverter from './converters/byte-converter';
import Ecoji from './converters/ecoji';
import LEB128 from './converters/leb-128';
import SimpleBase from './converters/simple-base';
import UUencode from './converters/uuencode';
import { SmartOutputCompileKey } from './io-handlers';

export declare class BaseEx<T extends SmartOutputCompileKey = 'buffer'> {
  base1: Base1<T>;
  base16: Base16<T>;
  base32_crockford: Base32<T>;
  base32_rfc3548: Base32<T>;
  base32_rfc4648: Base32<T>;
  base32_zbase32: Base32<T>;
  base58: Base58<T>;
  base58_bitcoin: Base58<T>;
  base58_flickr: Base58<T>;
  base64: Base64<T>;
  base64_urlsafe: Base64<T>;
  uuencode: UUencode<T>;
  uuencode_original: UUencode<T>;
  xxencode: UUencode<T>;
  base85_adobe: Base85<T>;
  base85_ascii: Base85<T>;
  base85_z85: Base85<T>;
  base91: Base91<T>;
  leb128: LEB128<T>;
  ecoji_v1: Ecoji<T>;
  ecoji_v2: Ecoji<T>;
  base2048: Base2048<T>;
  basePhi: BasePhi<T>;
  byteConverter: ByteConverter<T>;
  //  simpleBase: Record<string, SimpleBase<T>>;
  simpleBase: {
    base2: SimpleBase<T>;
    base3: SimpleBase<T>;
    base4: SimpleBase<T>;
    base5: SimpleBase<T>;
    base6: SimpleBase<T>;
    base7: SimpleBase<T>;
    base8: SimpleBase<T>;
    base9: SimpleBase<T>;
    base10: SimpleBase<T>;
    base11: SimpleBase<T>;
    base12: SimpleBase<T>;
    base13: SimpleBase<T>;
    base14: SimpleBase<T>;
    base15: SimpleBase<T>;
    base16: SimpleBase<T>;
    base17: SimpleBase<T>;
    base18: SimpleBase<T>;
    base19: SimpleBase<T>;
    base20: SimpleBase<T>;
    base21: SimpleBase<T>;
    base22: SimpleBase<T>;
    base23: SimpleBase<T>;
    base24: SimpleBase<T>;
    base25: SimpleBase<T>;
    base26: SimpleBase<T>;
    base27: SimpleBase<T>;
    base28: SimpleBase<T>;
    base29: SimpleBase<T>;
    base30: SimpleBase<T>;
    base31: SimpleBase<T>;
    base32: SimpleBase<T>;
    base33: SimpleBase<T>;
    base34: SimpleBase<T>;
    base35: SimpleBase<T>;
    base36: SimpleBase<T>;
    base37: SimpleBase<T>;
    base38: SimpleBase<T>;
    base39: SimpleBase<T>;
    base40: SimpleBase<T>;
    base41: SimpleBase<T>;
    base42: SimpleBase<T>;
    base43: SimpleBase<T>;
    base44: SimpleBase<T>;
    base45: SimpleBase<T>;
    base46: SimpleBase<T>;
    base47: SimpleBase<T>;
    base48: SimpleBase<T>;
    base49: SimpleBase<T>;
    base50: SimpleBase<T>;
    base51: SimpleBase<T>;
    base52: SimpleBase<T>;
    base53: SimpleBase<T>;
    base54: SimpleBase<T>;
    base55: SimpleBase<T>;
    base56: SimpleBase<T>;
    base57: SimpleBase<T>;
    base58: SimpleBase<T>;
    base59: SimpleBase<T>;
    base60: SimpleBase<T>;
    base61: SimpleBase<T>;
    base62: SimpleBase<T>;
  };

  constructor(outputType?: T);
}

export {
  Base1,
  Base16,
  Base32,
  Base58,
  Base64,
  UUencode,
  Base85,
  Base91,
  LEB128,
  Ecoji,
  Base2048,
  SimpleBase,
  BasePhi,
  ByteConverter,
};
