export type BytesInputType = ArrayLike<number> | ArrayBufferLike | ArrayBufferView | number;

export declare const BytesOutputTypeList = <const>['buffer', 'bytes', 'uint8', 'view'];
export type BytesOutputType = (typeof BytesOutputTypeList)[number];

export interface BytesOutputCompileReturn {
  buffer: ArrayBufferLike;
  view: DataView;
  bytes: ArrayBufferView;
  uint8: ArrayBufferView;
}
export type BytesOutputCompileKey = keyof BytesOutputCompileReturn;

export type SmartInputType = ArrayBuffer | ArrayBufferView | string | number | bigint;
export type SmartInputReturn<T> = T extends number
  ? 'int' | 'float'
  : T extends bigint
  ? 'int'
  : T extends ArrayBuffer | ArrayBufferView | string | SmartInputType[]
  ? 'bytes'
  : unknown;

export declare const SmartOutputTypeList = <const>[
  'bigint64',
  'bigint_n',
  'biguint64',
  'buffer',
  'bytes',
  'float32',
  'float64',
  'float_n',
  'int8',
  'int16',
  'int32',
  'int_n',
  'str',
  'uint8',
  'uint16',
  'uint32',
  'uint_n',
  'view',
];
export type SmartOutputType = (typeof SmartOutputTypeList)[number];

export interface SmartInputSetting {
  littleEndian: boolean;
  numberMode: boolean;
  signed: boolean;
}

export interface SmartOutputMakeTypedArrayReturn {
  int16: Int16Array;
  uint16: Uint16Array;
  int32: Int32Array;
  uint32: Uint32Array;
  float32: Float32Array;
  bigint64: BigInt64Array;
  biguint64: BigUint64Array;
  float64: Float64Array;
}
export type SmartOutputMakeTypedArrayKey = keyof SmartOutputMakeTypedArrayReturn;

export interface SmartOutputCompileReturn extends BytesOutputCompileReturn, SmartOutputMakeTypedArrayReturn {
  str: string;
  bigint_n: bigint;
  uint_n: number | bigint;
  int_n: number | bigint;
  float_n: number;
  number: number;
}
export type SmartOutputCompileKey = keyof SmartOutputCompileReturn;

export declare class BytesInput {
  static toBytes(input: BytesInputType): [Uint8Array, false, 'bytes'];
}

export declare class BytesOutput {
  static typeList: [...BytesOutputTypeList];
  static getType(type: BytesOutputType): BytesOutputType;
  static compile<T extends BytesOutputCompileKey>(Uint8ArrayOut: Uint8Array, type: T): BytesOutputCompileReturn[T];
}

export declare class SmartInput {
  static makeDataView(byteLen: number): DataView;
  static floatingPoints(input: number, littleEndian?: boolean): DataView;
  static numbers(input: number, littleEndian?: boolean): [Uint8Array, 'int' | 'float'];
  static bigInts(input: number | bigint, littleEndian?: boolean): Uint8Array;

  static toBytes<T extends SmartInputType | SmartInputType[]>(
    input: T,
    settings: SmartInputSetting
  ): [Uint8Array, boolean, SmartInputReturn<T>];
}

export declare class SmartOutput {
  static typeList: [...SmartOutputTypeList];
  static getType(type: SmartOutputType): SmartOutputType;

  static makeTypedArrayBuffer(
    Uint8ArrayOut: Uint8Array,
    bytesPerElem: number,
    littleEndian: boolean,
    negative: boolean
  ): ArrayBufferLike;

  static makeTypedArray<T extends SmartOutputMakeTypedArrayKey>(
    inArray: Uint8Array,
    type: T,
    littleEndian: boolean,
    negative: boolean
  ): SmartOutputMakeTypedArrayReturn[T];

  static compile<T extends SmartOutputCompileKey>(
    Uint8ArrayOut: Uint8Array,
    type: T,
    littleEndian?: boolean,
    negative?: boolean
  ): SmartOutputCompileReturn[T];
}
